const crypto = require('crypto');
const socketioJwt = require('socketio-jwt');

const { jwtSecret } = require('core/config'); // eslint-disable-line import/no-unresolved


function setPassword(password) {
  this.plainPassword = password;

  if (password) {
    this.salt = crypto.randomBytes(128).toString('base64');
    this.passwordHash = crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1').toString('base64');
  } else {
    this.salt = undefined;
    this.passwordHash = undefined;
  }
}

function getPassword() {
  return this.plainPassword;
}

function checkPassword(password) {
  if (!password) return false;
  if (!this.passwordHash) return false;
  return crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1').toString('base64') === this.passwordHash;
}

const socketAuth = socketioJwt.authorize({
  secret: jwtSecret,
  timeout: 15000,
});


module.exports = {
  setPassword,
  getPassword,
  checkPassword,
  socketAuth,
};
