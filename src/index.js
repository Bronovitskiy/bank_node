require('events').EventEmitter.defaultMaxListeners = 0;

const Koa = require('koa');
const socketIO = require('socket.io');
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger');
const cors = require('@koa/cors');
const passport = require('koa-passport');


const signUp = require('api/signUp');
const signIn = require('api/signIn');
const onAuthenticated = require('api/banks');

const { socketAuth } = require('utils/helpers/auth');

const { port } = require('core/config');

const app = new Koa();

app.use(logger());
app.use(bodyParser());
app.use(cors());
app.use(passport.initialize());
app.use(signUp.routes());
app.use(signIn.routes());

const server = app.listen(port);

require('./db');
require('./auth');

socketIO(server)
  .on('connection', socketAuth)
  .on('authenticated', onAuthenticated);


module.exports = app;
