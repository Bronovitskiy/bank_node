const Router = require('koa-router');
const jwt = require('jsonwebtoken');
const passport = require('koa-passport');

const { jwtSecret, tokenLifeTime } = require('core/config'); // eslint-disable-line import/no-unresolved

const router = new Router();

router.post('/sign-in', async (ctx, next) => {
  await passport.authenticate('local', (err, user, errorDescription = {}) => {
    if (!user) {
      const { errorMessage = 'Login failed' } = errorDescription;
      ctx.body = JSON.stringify({ errorMessage });
    } else {
      const payload = {
        id: user.id,
        displayName: user.displayName,
        email: user.email,
        exp: Math.round((Date.now() / 1000) + tokenLifeTime),
      };
      const token = jwt.sign(payload, jwtSecret);

      ctx.body = { user: user.displayName, token };
    }
  })(ctx, next);
});

module.exports = router;
