const Router = require('koa-router');

const User = require('models/user'); // eslint-disable-line import/no-unresolved

const router = new Router();

router.post('/sign-up', async (ctx) => {
  try {
    ctx.body = await User.create(ctx.request.body);
  } catch (err) {
    ctx.status = 400;
    ctx.body = err;
  }
});

module.exports = router;
