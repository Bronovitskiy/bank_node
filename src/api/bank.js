const Router = require('koa-router');

const Bank = require('models/bank'); // eslint-disable-line import/no-unresolved

const router = new Router();

router.post('/banks', async (ctx) => {
    try {
        ctx.body = await Bank.find(ctx.request.id);
    } catch (err) {
        ctx.status = 404;
        ctx.body = err;
    }
});

module.exports = router;
