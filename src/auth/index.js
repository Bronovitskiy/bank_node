const passport = require('koa-passport');
const LocalStrategy = require('passport-local');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');

const { jwtSecret } = require('core/config');
const User = require('models/user');


passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  session: false,
},
((email, password, done) => {
  User.findOne({ email }, (err, user) => {
    if (err) {
      return done(err);
    }

    if (!user || !user.checkPassword(password)) {
      return done(null, false, { errorMessage: 'User does not exist or wrong password.' });
    }

    return done(null, user);
  });
})));


const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeader(),
  secretOrKey: jwtSecret,
};

passport.use(new JwtStrategy(jwtOptions, ((payload, done) => {
  User.findById(payload.id, (err, user) => {
    if (err) {
      done(err);
    }
    if (!err && user) {
      done(null, user);
    } else {
      done(null, false);
    }
  });
})));
