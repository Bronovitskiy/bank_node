const mongoose = require('mongoose');

const { url, options } = require('core/config'); // eslint-disable-line import/no-unresolved

mongoose.set('debug', true);

mongoose.connect(url, options);

const db = mongoose.connection;

db.on('error', console.error);

db.once('open', () => console.log('Connected to DB!'));

module.exports = mongoose;
