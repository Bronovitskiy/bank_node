const mongoose = require('mongoose');

const bankSchema = new mongoose.Schema({
    displayName: String,
}, {
    timestamps: true,
});

module.exports = mongoose.model('Bank', bankSchema);
