const mongoose = require('mongoose');
const { setPassword, getPassword, checkPassword } = require('utils/helpers/auth');

const userSchema = new mongoose.Schema({
  displayName: String,
  email: {
    type: String,
    required: 'e-mail is required',
    unique: 'this e-mail already exist',
  },
  passwordHash: String,
}, {
  timestamps: true,
});

userSchema.virtual('password')
  .set(setPassword)

  .get(getPassword);

userSchema.methods.checkPassword = checkPassword;

module.exports = mongoose.model('User', userSchema);
