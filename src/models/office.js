const mongoose = require('mongoose');

const officeSchema = new mongoose.Schema({
    name: String,
    address: String,
    exchangeRates: String,
    lastUpdate: String
}, {
    timestamps: true,
});

module.exports = mongoose.model('Office', officeSchema);
