const path = require('path');
const fs = require('fs');

const nodeModules = {};
fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach((mod) => {
    nodeModules[mod] = `commonjs ${mod}`;
  });

module.exports = {
  entry: './src/index.js',
  target: 'node',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'backend.js',
  },
  externals: nodeModules,
  resolve: {
    alias: {
      api: path.resolve(__dirname, 'src/api/'),
      auth: path.resolve(__dirname, 'src/auth/'),
      db: path.resolve(__dirname, 'src/db/'),
      models: path.resolve(__dirname, 'src/models/'),
      utils: path.resolve(__dirname, 'src/utils/'),
      core: path.resolve(__dirname, 'src/core/'),
    },
    extensions: ['.js', '.jsx'],
  },
};
